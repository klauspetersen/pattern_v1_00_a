/*
 * subject.h
 *
 *  Created on: 28/03/2013
 *      Author: Iris
 */

#ifndef SUBJECT_H_
#define SUBJECT_H_

#define subjFREE(ptr) free(ptr)
#define subjMALLOC(size) malloc(size)
#define subjVERBOSE 1

#if subjVERBOSE == 1
  #define subjPRINT(a) printf a
#else
  #define subjPRINT(a) (void)0
#endif


typedef struct subj subj;

subj *subj_create(void);
int subj_subscribe(subj *this, void *func);
int subj_unsubscribe(subj *this, void *func);
int subj_notify(subj *node, void *arg);


#endif /* SUBJECT_H_ */
