/*
 * testbench.c
 *
 *  Created on: 28/03/2013
 *      Author: Iris
 */

#include <stdio.h>
#include <stdlib.h>
#include "subject.h"

void test1(  int i  );
void test2(  int i  );
void test3(  int i  );
void test4(  int i  );

subj *pSubj;

int main(void) {
	printf("Test\r\n");
	pSubj = subj_create();
	subj_subscribe(pSubj, test1);
	subj_subscribe(pSubj, test2);
	subj_subscribe(pSubj, test3);
	subj_subscribe(pSubj, test4);
	subj_notify(pSubj, (void *)10);

	subj_unsubscribe(pSubj, test1);
	subj_unsubscribe(pSubj, test3);
	subj_unsubscribe(pSubj, test2);
	subj_unsubscribe(pSubj, test4);
	subj_unsubscribe(pSubj, test4);

	subj_subscribe(pSubj, test1);
	subj_subscribe(pSubj, test2);
	subj_subscribe(pSubj, test3);
	subj_unsubscribe(pSubj, test3);
	subj_notify(pSubj, (void *)10);

	return EXIT_SUCCESS;
}

void test1( int i ) {
	printf("test1! %d\r\n", i);
}

void test2( int i ) {
	printf("test2! %d\r\n", i);
}
void test3( int i ) {
	printf("test3! %d\r\n", i);
}

void test4( int i ) {
	printf("test4! %d\r\n", i);
}
